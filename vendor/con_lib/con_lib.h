#ifndef CON_LIB_H
#define CON_LIB_H

// Spalvos
#define COLOR_BLACK     0
#define COLOR_RED       1
#define COLOR_GREEN     2
#define COLOR_ORANGE    3
#define COLOR_BLUE      4
#define COLOR_MAGENTA   5
#define COLOR_CYAN      6
#define COLOR_GRAY      7

// Iإ،valo ekranؤ…
void con_clear();

// Nuskaito vienؤ… klaviإ،ؤ…. 
// Graإ¾ina 0, jei nؤ—ra ko daugiau skaityti
int con_read_key();

// Nustato fono ir teksto spalvؤ…
// * bg - fono spalva (COLOR_*)
// * fg - teksto spalva (COLOR_*)
void con_set_color(int bg, int fg);

// Nustato dabartinؤ™ iإ،vedimo pozicijؤ…. x, y - koordinatؤ—s:
// * virإ،utinis kairys terminalo kampas yra (0, 0)
// * x-ai didؤ—ja iإ، kairؤ—s ؤ¯ deإ،inؤ™
// * y-ai didؤ—ja iإ، virإ،aus ؤ¯ apaؤچiؤ…
void con_set_pos(int x, int y);

// Nustato cursoriaus rodymo rؤ—إ¾imؤ….
// * Jei show == 0, cursoriإ³ paslepia
// * Jei show == 1, cursoriإ³ rodo
void con_show_cursor(int show);

// Padaro, kad nesimatytإ³ ؤ¯vedamإ³ simboliإ³ (tik LINUX os)
// * Jei show == 0, tai ؤ¯vedamإ³ simboliإ³ neatkartoja ؤ¯ ekranؤ…
// * Jei show == 1, tai ؤ¯vedamus simbolius rodo 
// * Ant Windows nieko nedaro 
void con_show_echo(int show);

// Miega nurodytؤ… sekundإ¾iإ³ skaiؤچiإ³.
// * seconds turi bإ«ti intervale [0.01; 100.0]
void con_sleep(float seconds);

#endif // CON_LIB_H