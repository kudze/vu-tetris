//
// Created by karol on 2019-11-13.
//

#ifndef TETRIS_RENDERER_H
#define TETRIS_RENDERER_H

/**
 * Is called after every game tick.
 * Handles rendering of the game.
 **/
void render(double delta);

void set_leaderboard_rendering_enabled(bool enabled);
bool is_leaderboard_rendered();

#endif //TETRIS_RENDERER_H
