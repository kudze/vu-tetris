#include "stdafx.h"

#define TARGET_FRAME_RATE 100
#define SECONDS_PER_FRAME 1.0f / TARGET_FRAME_RATE

clock_t deltaClock;
void delta_clock_init() {
    deltaClock = clock();
}

float delta_clock_calculate() {
    clock_t tempClock = clock();

    float delta = (float) (tempClock - deltaClock) / CLOCKS_PER_SEC;

    deltaClock = tempClock;

    return delta;
}

void print_usage(const char* filename) {
    printf("Usage: %s <gamename>\n", filename);
}

//False on failure.
bool parse_usage(int argc, char* argv[], char* gamename_out)
{
    if(argc != 2 || strlen(argv[1]) == 0 || strlen(argv[1]) >= GAME_TITLE_MAX_LENGTH) {
        print_usage(argv[0]);
        system("pause");

        return false;
    }

    strncpy(gamename_out, argv[1], GAME_TITLE_MAX_LENGTH);

    return true;
}

void cleanup();

/**
 * Called before any game ticks have passed.
 **/
void init(int argc, char* argv[]) {
    logger_init();

    con_clear();

    //Checks usage.
    char title[GAME_TITLE_MAX_LENGTH];
    if(!parse_usage(argc, argv, title))
        exit(1);

    leaderboard_init();
    input_init();
    game_init(title);
    delta_clock_init();
}

/**
 * Called on termination of the process.
 **/
bool cleanupped = false;
void cleanup() {
    if(!cleanupped) {
        cleanupped = true;

        input_deinit();

        con_clear();

        LeaderBoardRow row;
        strcpy(row.player_name, game_get_title());
        row.score = game_get_score();

        printf("Game Over! Your score was %d! You placed %d in the leaderboard!\n", row.score,
               leaderboard_push_game_data(row));
        system("PAUSE");

        leaderboard_deinit();
        logger_deinit();
    }
}


#ifndef UNIT_TEST
/**
 * Entry point of the program.
 **/
int main(int argc, char* argv[]) {
    init(argc, argv);

    while (!game_has_ended()) {
        float delta = delta_clock_calculate();

        input_tick();
        game_tick(delta);
        render(delta);

        if(delta < SECONDS_PER_FRAME)
            con_sleep(fmaxf(SECONDS_PER_FRAME - delta, 0.1f));
    }

    cleanup();

    return 1;
}

#endif