#include "stdafx.h"

void board_remove_row(coord_t y) {

    //Lets remove the row.
    for (coord_t x = 0; x < BOARD_WIDTH; x++)
        board_remove_block_at(x, y);

    //Lets move other rows one row below. And delete top most one.
    if (y < BOARD_HEIGHT)
        for (coord_t _y = y; _y < BOARD_HEIGHT; _y++)
            for (coord_t x = 0; x < BOARD_WIDTH; x++)
                board_set_block_color_at(
                    x,
                    _y,
                    _y != (BOARD_HEIGHT - 1) ?
                    board_get_block_color_at(x, _y + 1) : GAME_BLOCK_COLOR_NONE
                );

}

int removedTotal = 0;

void board_check_for_full_rows() {
    int removedCount = 0;

    for (int y = 0; y < BOARD_HEIGHT; y++) {
        bool shouldBeRemoved = true;

        for (int x = 0; x < BOARD_WIDTH; x++) {
            if (board_is_position_free(x, y)) {
                shouldBeRemoved = false;
                break;
            }
        }

        if (shouldBeRemoved) {
            board_remove_row(y);

            removedCount++;

            //We want to rescan this line.
            //Since all lines will on top of this will drop by one.
            y--;
        }
    }

    if (removedCount > 0) {
        //Scoring.
        game_add_score(100 * removedCount);

        //Level upping.
        removedTotal += removedCount;
        while (removedTotal > GAME_SHAPE_DROP_RATE_INCREASES_AFTER_X_LINES) {
            removedTotal -= GAME_SHAPE_DROP_RATE_INCREASES_AFTER_X_LINES;

            game_set_shape_drop_rate(
                fabs(
                    game_get_shape_drop_rate() - GAME_SHAPE_DROP_RATE_INCREASES_BY
                )
            );
        }
    }
}

game_block_colour_t game_generate_random_block_color() {
    return rand() % GAME_BLOCK_COLOR_MAX;
}

coord_t game_calculate_shape_width(Shape const *shape) {
    if(shape == NULL)
        return 0;

    coord_t minX = COORD_T_MAX;
    coord_t maxX = COORD_T_MIN;

    for (int i = 0; i < BLOCK_COUNT_PER_SHAPE; i++) {
        minX = min(minX, shape->block_coordinates[i].x);
        maxX = max(maxX, shape->block_coordinates[i].x);
    }

    return maxX - minX + 1;
}

coord_t game_calculate_shape_height(Shape const *shape) {
    if(shape == NULL)
        return 0;

    coord_t minY = COORD_T_MAX;
    coord_t maxY = COORD_T_MIN;

    for (int i = 0; i < BLOCK_COUNT_PER_SHAPE; i++) {
        minY = min(minY, shape->block_coordinates[i].y);
        maxY = max(maxY, shape->block_coordinates[i].y);
    }

    return maxY - minY + 1;
}

Point game_calculate_shape_dimensions(Shape const *shape) {
    Point result;

    result.x = game_calculate_shape_width(shape);
    result.y = game_calculate_shape_height(shape);

    return result;
}

double shape_drop_rate = GAME_SHAPE_DROP_RATE_DEFAUT;

double game_get_shape_drop_rate() {
    return shape_drop_rate;
}

void game_set_shape_drop_rate(double rate) {
    shape_drop_rate = rate;
}

Shape current_dropping_shape = {
    {
        {0, 0},
        {0, 0},
        {0, 0},
        {0, 0}
    }
};

bool game_exists_current_dropping_shape() {
    for (int i = 0; i < BLOCK_COUNT_PER_SHAPE; i++) {
        Point *point = &current_dropping_shape.block_coordinates[i];

        if (point->x != 0 || point->y != 0)
            return true;
    }

    return false;
}

Shape *game_get_current_dropping_shape() {
    return &current_dropping_shape;
}

Shape shapeLibrary[GAME_SHAPE_MAX] = {
//GAME_SHAPE_I 0 //4x1
    {
        {
            {0, 0},
            {1, 0},
            {2, 0},
            {3, 0}
        },
        {1, 0},
        GAME_SHAPE_I,
    },
//GAME_SHAPE_O 1 //2x2
    {
        {
            {0, 0},
            {1, 0},
            {0, 1},
            {1, 1}
        },
        {1, 0},
        GAME_SHAPE_O
    },
//GAME_SHAPE_T 2 //One mid x three line
    {
        {
            {1, 0},
            {0, 1},
            {1, 1},
            {2, 1}
        },
        {1, 1},
        GAME_SHAPE_T
    },
//GAME_SHAPE_J 3 //One left bot three line top.
    {
        {
            {0, 0},
            {1, 0},
            {2, 0},
            {0, 1}
        },
        {0, 1},
        GAME_SHAPE_J
    },
//GAME_SHAPE_L 4 //One right bot three line top
    {
        {
            {0, 0},
            {1, 0},
            {2, 0},
            {2, 1}
        },
        {0, 1},
        GAME_SHAPE_L
    },
//GAME_SHAPE_S 5 //2x2 (not alligned)
    {
        {
            {0, 0},
            {1, 0},
            {1, 1},
            {2, 1}
        },
        {1, 1},
        GAME_SHAPE_S
    },
//GAME_SHAPE Z 6 //2x2 (not alligned) mirrored
    {
        {
            {1, 0},
            {2, 0},
            {0, 1},
            {1, 1}
        },
        {1, 1},
        GAME_SHAPE_Z
    },
};

Shape const *game_get_shape_of_index(game_shape_t index) {
    return &shapeLibrary[index];
}

bool game_is_pos_in_shape(Shape const *shape, coord_t x, coord_t y) {
    for (int i = 0; i < BLOCK_COUNT_PER_SHAPE; i++)
        if (shape->block_coordinates[i].x == x && shape->block_coordinates[i].y == y)
            return true;

    return false;
}

bool game_is_pos_in_shape_until_index(Shape const *shape, coord_t x, coord_t y, unsigned int index) {
    //index gets floored to BLOCK_COUNR_PER_SHAPE and ceiled into 0
    index = max(0, min((int) index, BLOCK_COUNT_PER_SHAPE));

    for (int i = 0; i < index; i++)
        if (shape->block_coordinates[i].x == x && shape->block_coordinates[i].y == y)
            return true;

    return false;
}

int game_generate_random_shape() {
    return rand() % GAME_SHAPE_MAX;
}

void game_generate_current_shape(game_shape_t shape, game_block_colour_t color) {
    Shape const *shapeToSpawn = game_get_shape_of_index(shape);
    Point shapeDimensions = game_calculate_shape_dimensions(shapeToSpawn);
    Shape *shapeToUpdate = game_get_current_dropping_shape();

    coord_t offsetX = random(0, BOARD_WIDTH - shapeDimensions.x);
    coord_t offsetY = BOARD_HEIGHT - shapeDimensions.y;

    logger_log_va("Spawning a shape of %d index at (%d:%d) offset!", shape, offsetX, offsetY);

    for (int i = 0; i < BLOCK_COUNT_PER_SHAPE; i++) {
        coord_t x = offsetX + shapeToSpawn->block_coordinates[i].x;
        coord_t y = offsetY + shapeToSpawn->block_coordinates[i].y;
        board_set_block_color_at(x, y, color);

        shapeToUpdate->block_coordinates[i].x = x;
        shapeToUpdate->block_coordinates[i].y = y;
    }

    shapeToUpdate->rotation_anchor.x = shapeToSpawn->rotation_anchor.x + offsetX;
    shapeToUpdate->rotation_anchor.y = shapeToSpawn->rotation_anchor.y + offsetY;
    shapeToUpdate->shapeIndex = shapeToSpawn->shapeIndex;
}

void game_generate_random_current_dropping_shape() {
    game_generate_current_shape(
        game_generate_random_shape(),
        game_generate_random_block_color()
    );
}

void game_update_current_shape() {
    if (game_can_current_shape_be_lowered())
        game_lower_current_shape();

    else
        game_clear_current_shape();
}

void game_clear_current_shape() {
    Shape *shapeToClear = game_get_current_dropping_shape();

    for (int i = 0; i < BLOCK_COUNT_PER_SHAPE; i++) {
        shapeToClear->block_coordinates[i].x = 0;
        shapeToClear->block_coordinates[i].y = 0;
    }
}

bool game_can_current_shape_be_translated(s_coord_t x, s_coord_t y) {
    Shape *shapeToTranslate = game_get_current_dropping_shape();

    for (int i = 0; i < BLOCK_COUNT_PER_SHAPE; i++) {
        int nextX = shapeToTranslate->block_coordinates[i].x + x;
        int nextY = shapeToTranslate->block_coordinates[i].y + y;

        if (!board_is_position_free(nextX, nextY) && !game_is_pos_in_shape(shapeToTranslate, nextX, nextY))
            return false;
    }

    return true;
}

void game_translate_current_shape(s_coord_t x, s_coord_t y) {
    Shape *shapeToTranslate = game_get_current_dropping_shape();
    game_block_colour_t block = GAME_BLOCK_COLOR_NONE;

    for (int i = 0; i < BLOCK_COUNT_PER_SHAPE; i++) {
        coord_t oldX = shapeToTranslate->block_coordinates[i].x;
        coord_t oldY = shapeToTranslate->block_coordinates[i].y;

        if (i == 0)
            block = board_get_block_color_at(oldX, oldY);

        coord_t newX = oldX + x;
        coord_t newY = oldY + y;

        if (!game_is_pos_in_shape_until_index(shapeToTranslate, oldX, oldY, i))
            board_remove_block_at(oldX, oldY);

        board_set_block_color_at(newX, newY, block);

        shapeToTranslate->block_coordinates[i].x = newX;
        shapeToTranslate->block_coordinates[i].y = newY;
    }

    shapeToTranslate->rotation_anchor.x += x;
    shapeToTranslate->rotation_anchor.y += y;
}

bool game_can_current_shape_shift_to_left() {
    return game_can_current_shape_be_translated(-1, 0);
}

void game_shift_current_shape_to_left() {
    game_translate_current_shape(-1, 0);
}

bool game_can_current_shape_shift_to_right() {
    return game_can_current_shape_be_translated(1, 0);
}

void game_shift_current_shape_to_right() {
    game_translate_current_shape(1, 0);
}

bool game_can_current_shape_be_lowered() {
    return game_can_current_shape_be_translated(0, -1);
}

void game_lower_current_shape() {
    game_translate_current_shape(0, -1);
}

void game_drop_current_shape() {

    while (game_can_current_shape_be_lowered())
        game_lower_current_shape();

}

bool game_can_current_shape_rotate(ROTATION_STATE rotation) {
    Shape *shapeToRotate = game_get_current_dropping_shape();

    if (shapeToRotate->shapeIndex == GAME_SHAPE_O)
        return false;

    for (int i = 0; i < BLOCK_COUNT_PER_SHAPE; i++) {
        int oldX = shapeToRotate->block_coordinates[i].x;
        int oldY = shapeToRotate->block_coordinates[i].y;

        coord_t oldX_normalized = oldX - shapeToRotate->rotation_anchor.x;
        coord_t oldY_normalized = oldY - shapeToRotate->rotation_anchor.y;

        if (rotation == ROTATION_STATE_RIGHT)
            oldY_normalized *= -1;

        coord_t newX = -oldY_normalized;
        coord_t newY = oldX_normalized;

        if (rotation == ROTATION_STATE_RIGHT)
            newY *= -1;

        else if (rotation == ROTATION_STATE_INVERSE) {
            coord_t tempX = newX;
            newX = -newY;
            newY = tempX;
        }

        newX += shapeToRotate->rotation_anchor.x;
        newY += shapeToRotate->rotation_anchor.y;

        if (!board_is_position_free(newX, newY) && !game_is_pos_in_shape(shapeToRotate, newX, newY))
            return false;
    }

    return true;
}

void game_rotate_current_shape(ROTATION_STATE rotation) {
    Shape *shapeToRotate = game_get_current_dropping_shape();
    game_block_colour_t color = GAME_BLOCK_COLOR_NONE;

    //Clear current coords.
    for (int i = 0; i < BLOCK_COUNT_PER_SHAPE; i++) {
        //Lets get a block color before we remove the shape.
        if (i == 0)
            color = board_get_block_color_at(
                shapeToRotate->block_coordinates[i].x,
                shapeToRotate->block_coordinates[i].y
            );

        board_set_block_color_at(
            shapeToRotate->block_coordinates[i].x,
            shapeToRotate->block_coordinates[i].y,
            GAME_BLOCK_COLOR_NONE
        );
    }

    //Calculates and updates everything.
    for (int i = 0; i < BLOCK_COUNT_PER_SHAPE; i++) {
        int oldX = shapeToRotate->block_coordinates[i].x;
        int oldY = shapeToRotate->block_coordinates[i].y;

        coord_t oldX_normalized = oldX - shapeToRotate->rotation_anchor.x;
        coord_t oldY_normalized = oldY - shapeToRotate->rotation_anchor.y;

        if (rotation == ROTATION_STATE_RIGHT)
            oldY_normalized *= -1;

        coord_t newX = -oldY_normalized;
        coord_t newY = oldX_normalized;

        if (rotation == ROTATION_STATE_RIGHT)
            newY *= -1;

        else if (rotation == ROTATION_STATE_INVERSE) {
            coord_t tempX = newX;
            newX = -newY;
            newY = tempX;
        }

        newX += shapeToRotate->rotation_anchor.x;
        newY += shapeToRotate->rotation_anchor.y;

        shapeToRotate->block_coordinates[i].x = newX;
        shapeToRotate->block_coordinates[i].y = newY;

        board_set_block_color_at(newX, newY, color);
    }
}

void game_on_shape_drop_tick() {
    if (!game_exists_current_dropping_shape()) {
        board_check_for_full_rows();

        game_generate_random_current_dropping_shape();

        if (!game_can_current_shape_be_lowered()) {
            game_stop_on_next_frame();

            archive_remove();
        }

    } else
        game_update_current_shape();
}

/**
 * Responsible for handling user input.
 */
void game_on_key_pressed(char key) {
    switch (key) {
        case 'a':
            if (game_can_current_shape_shift_to_left() && !game_is_paused())
                game_shift_current_shape_to_left();

            break;
        case 'd':
            if (game_can_current_shape_shift_to_right() && !game_is_paused())
                game_shift_current_shape_to_right();

            break;
        case 's':
            if (game_can_current_shape_be_lowered() && !game_is_paused())
                game_lower_current_shape();

            break;
        case ' ':
            if (game_can_current_shape_be_lowered() && !game_is_paused())
                game_drop_current_shape();

            break;
        case ',':
            if (game_can_current_shape_rotate(ROTATION_STATE_LEFT) && !game_is_paused())
                game_rotate_current_shape(ROTATION_STATE_LEFT);

            break;
        case '.':
            if (game_can_current_shape_rotate(ROTATION_STATE_RIGHT) && !game_is_paused())
                game_rotate_current_shape(ROTATION_STATE_RIGHT);

            break;
        case 'x':
            archive_save();

            break;
        case 'p':
            game_set_paused(
                !game_is_paused()
            );

            break;
        case 'l':
            set_leaderboard_rendering_enabled(
                !is_leaderboard_rendered()
            );

            break;
        default:
            break;
    }
}

score_t score = 0;

score_t game_get_score() {
    return score;
}

void game_add_score(score_t _score) {
    score += _score;
}

void game_set_score(score_t _score) {
    score = _score;
}

char game_title[GAME_TITLE_MAX_LENGTH];

const char *game_get_title() {
    return game_title;
}

bool paused = false;

bool game_is_paused() {
    return paused;
}

void game_set_paused(bool _paused) {
    paused = _paused;
}

bool is_game_running = true;

bool game_has_ended() {
    return !is_game_running;
}

void game_stop_on_next_frame() {
    is_game_running = false;
}

void game_init(const char *title) {
    srand(time(NULL));

    strcpy(game_title, title);

    input_register_listener(game_on_key_pressed);

    archive_try_load();
}

double deltaSum = 0;

void game_tick(double delta) {
    if (paused)
        return;

    //Game Shape update.
    deltaSum += delta;
    while (floor(deltaSum / shape_drop_rate) != 0) {
        deltaSum -= shape_drop_rate;

        game_on_shape_drop_tick();
    }
}