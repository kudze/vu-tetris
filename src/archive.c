#include "stdafx.h"

#define ARCHIVE_SAVE_FILE_NAME_LENGTH GAME_TITLE_MAX_LENGTH + 4
//mode must be of length ARCHIVE_SAVE_FILE_NAME_LENGTH
void buildSaveFileName(char* mode) {
    sprintf(mode, "%s.bin", game_get_title());
}

FILE* openSaveStream(const char* mode)
{
    char filename[ARCHIVE_SAVE_FILE_NAME_LENGTH];
    buildSaveFileName(filename);

    return fopen(filename, mode);
}

void archive_try_load() {
    FILE *file = openSaveStream("rb");

    if (!file)
        return;

    score_t score;
    Shape *shape = game_get_current_dropping_shape();
    d_coord_t boardLength;

    fread(&score, sizeof(score_t), 1, file);
    fread(shape, sizeof(Shape), 1, file);

    fread(&boardLength, sizeof(d_coord_t), 1, file);
    BlockData* boardData = malloc(sizeof(BlockData) * boardLength);
    fread(boardData, sizeof(BlockData), boardLength, file);

    fclose(file);

    game_set_score(score);
    board_init_data(boardData, boardLength);

    free(boardData);

    logger_info("Save loaded!");
}

void archive_save() {
    FILE *file = openSaveStream("wb");

    score_t score = game_get_score();
    Shape *shape = game_get_current_dropping_shape();
    d_coord_t boardLength;
    BlockData const* boardData = board_get_data(&boardLength);

    fwrite(&score, sizeof(score_t), 1, file);
    fwrite(shape, sizeof(Shape), 1, file);
    fwrite(&boardLength, sizeof(d_coord_t), 1, file);
    fwrite(boardData, sizeof(BlockData), boardLength, file);

    fclose(file);

    logger_info("Save saved!");
}

void archive_remove() {
    char filename[ARCHIVE_SAVE_FILE_NAME_LENGTH];
    buildSaveFileName(filename);

    remove(filename);
}