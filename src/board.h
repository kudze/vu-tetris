//
// Created by karol on 2019-12-12.
//

#ifndef TETRIS_BOARD_H
#define TETRIS_BOARD_H

#define BOARD_WIDTH 10
#define BOARD_HEIGHT 28

#define COORD_T_MIN 0
#define COORD_T_MAX UCHAR_MAX
typedef uint8_t coord_t;
typedef uint16_t d_coord_t;
typedef int8_t s_coord_t; //signed version of coord_t
typedef struct {
    coord_t y;
    coord_t x;
} Point;

/**
 * Dynamic board allocation
 */
typedef union {
    d_coord_t mapped_coords;
    Point point;
} PositionData;

typedef struct {
    PositionData position;
    game_block_colour_t colour;
} BlockData;

BlockData const* board_get_data(d_coord_t* length);
void board_init_data(BlockData const* data, d_coord_t length);

/**
 * Returns NULL ptr if block doesn't exist at x or y.
 */
bool board_is_x_valid(coord_t x);
bool board_is_y_valid(coord_t y);
bool board_is_position_free(coord_t x, coord_t y);
bool board_is_position_free_point(Point point);
game_block_colour_t board_get_block_color_at(coord_t x, coord_t y);
game_block_colour_t board_get_block_color_at_point(Point point);
void board_set_block_color_at(coord_t x, coord_t y, game_block_colour_t color);
void board_set_block_color_at_point(Point point, game_block_colour_t color);
void board_remove_block_at(coord_t x, coord_t y);
void board_remove_block_at_point(Point point);
void board_remove_row(coord_t y);
void board_check_for_full_rows(); //Checks if there is any full rows and deletes them.

#endif //TETRIS_BOARD_H
