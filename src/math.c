//
// Created by karol on 2019-11-24.
//

#include <stdlib.h>

#include "math.h"

unsigned int umin(unsigned int a, unsigned int b) {
    return a > b ? b : a;
}

unsigned int umax(unsigned int a, unsigned int b) {
    return a < b ? b : a;
}

int min(int a, int b) {
    return a > b ? b : a;
}

int max(int a, int b) {
    return a < b ? b : a;
}

int random(int min, int max) {
    int diff = max - min + 1;

    return (rand() % diff) + min;
}