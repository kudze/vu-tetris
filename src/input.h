#ifndef INPUT_H
#define INPUT_H

#include <stdbool.h>

void input_init();
void input_tick();
void input_deinit();

typedef void(*input_listener_ft)(char key);
void input_register_listener(input_listener_ft listener);

#endif