#ifndef STDAFX_H
#define STDAFX_H

//#define UNIT_TEST

//C Libraries.
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <string.h>
#include <assert.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>
#include <stdint.h>
#include <stdarg.h>

//Vendor libraries.
#include "con_lib.h"
#include "vec.h"

//TETRIS
#include "math.h"
#include "input.h"
#include "logic.h"
#include "renderer.h"
#include "archive.h"
#include "log.h"
#include "leaderboard.h"

#endif //STDAFX_H