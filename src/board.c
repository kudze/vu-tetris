#include <stdint.h>
#include "stdafx.h"

/**
 * Dynamic Allocation stuff.
 */

BlockData* blockData = NULL;
d_coord_t blockDataLength = 0;

void swap_block_data(BlockData* a, BlockData* b)
{
    BlockData temp = *a;
    *a = *b;
    *b = temp;
}

bool board_is_initialized() {
    return blockData != NULL;
}

d_coord_t board_convert_coord_to_offset(coord_t x, coord_t y)
{
    PositionData temp;

    temp.point.x = x;
    temp.point.y = y;

    return temp.mapped_coords;
}

BlockData* board_search_block_data(d_coord_t value)
{
    if(!board_is_initialized())
        return NULL;

    //binary search
    d_coord_t left = 0;
    d_coord_t right = blockDataLength - (d_coord_t)1;
    while(left <= right && right != UINT16_MAX && left != blockDataLength) {
        d_coord_t mid = (d_coord_t) ceilf((left + right) / 2.f);

        if (blockData[mid].position.mapped_coords == value)
            return &blockData[mid];

        if (blockData[mid].position.mapped_coords < value)
            left = mid + 1;

        else
            right = mid - 1;
    }

    return NULL;
}

BlockData const* board_get_data(d_coord_t* length) {
    *length = blockDataLength;

    return blockData;
}

void board_init_data(BlockData const* data, d_coord_t length) {
    if(board_is_initialized())
        free(blockData);

    blockData = malloc(sizeof(BlockData) * length);
    memcpy(blockData, data, sizeof(BlockData) * length);

    blockDataLength = length;
}

/**
 * BOARD LOGIC.
 **/

bool board_is_x_valid(coord_t x) {
    return x < BOARD_WIDTH;
}

bool board_is_y_valid(coord_t y) {
    return y < BOARD_HEIGHT;
}

bool board_is_position_free(coord_t x, coord_t y) {
    if (!board_is_x_valid(x))
        return false;

    if (!board_is_y_valid(y))
        return false;

    return board_get_block_color_at(x, y) == GAME_BLOCK_COLOR_NONE;
}

bool board_is_position_free_point(Point point)
{
    return board_is_position_free(point.x, point.y);
}

game_block_colour_t board_get_block_color_at(coord_t x, coord_t y) {
    if (!board_is_x_valid(x))
        return GAME_BLOCK_COLOR_NONE;

    if (!board_is_y_valid(y))
        return GAME_BLOCK_COLOR_NONE;

    d_coord_t offset = board_convert_coord_to_offset(x, y);
    BlockData* data = board_search_block_data(offset);

    if(data == NULL)
        return GAME_BLOCK_COLOR_NONE;

    return data->colour;
}

game_block_colour_t board_get_block_color_at_point(Point point)
{
    return board_get_block_color_at(point.x, point.y);
}

void board_set_block_color_at(coord_t x, coord_t y, game_block_colour_t colour) {
    if (!board_is_x_valid(x))
        return;

    if (!board_is_y_valid(y))
        return;

    if(colour == GAME_BLOCK_COLOR_NONE)
        board_remove_block_at(x, y);

    d_coord_t offset = board_convert_coord_to_offset(x, y);
    BlockData* _block = board_search_block_data(offset);

    //If a block exists with this offset then its simple, we only need to replace existing pointer.
    if(_block != NULL) {
        _block->colour = colour;

        return;
    }

    //Otherwise we increase blockDataLength by one.
    blockDataLength++;

    //If list isnt initialized yet we need to allocate it and insert our first block.
    if(!board_is_initialized()) {
        blockData = malloc(sizeof(BlockData) * blockDataLength);

        _block = &blockData[blockDataLength - 1];
        _block->position.mapped_coords = offset;
        _block->colour = colour;

        return;
    }

    //Otherwise we need to add the element to array and sort it.
    blockData = realloc(blockData, sizeof(BlockData) * blockDataLength);
    _block = &blockData[blockDataLength - 1];
    _block->position.mapped_coords = offset;
    _block->colour = colour;

    //And one reversed iteration of bubble sort should sort it.
    for(d_coord_t i = blockDataLength - 1; i --> 0;) {
        if (blockData[i].position.mapped_coords > blockData[i + 1].position.mapped_coords)
            swap_block_data(&blockData[i], &blockData[i + 1]);
        else break; //If its sorted we just quit.
    }

    int i = 0;
}

void board_set_block_color_at_point(Point point, game_block_colour_t block) {
    return board_set_block_color_at(point.x, point.y, block);
}

void board_remove_block_at(coord_t x, coord_t y) {
    d_coord_t offset = board_convert_coord_to_offset(x, y);
    BlockData* blockToRemove = board_search_block_data(offset);

    if(blockToRemove != NULL) {
        //Calculating the offset of the element from the start of the array.
        int i = blockToRemove - blockData;

        //Let's move that element to the back.
        for(; i < blockDataLength - 1; i++)
            swap_block_data(&blockData[i], &blockData[i + 1]);

        //Lets resize the array and remove the element from the array.
        blockDataLength--;
        blockData = realloc(blockData, sizeof(BlockData) * blockDataLength);
    }
}

void board_remove_block_at_point(Point point) {
    board_remove_block_at(point.x, point.y);
}