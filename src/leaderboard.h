//
// Created by karol on 2019-12-15.
//

#ifndef TETRIS_LEADERBOARD_H
#define TETRIS_LEADERBOARD_H

typedef struct {
    char player_name[GAME_TITLE_MAX_LENGTH];
    score_t score;
} LeaderBoardRow;

/**
 * caches leaderboard's data.
 */
void leaderboard_init();
void leaderboard_deinit();

LeaderBoardRow* leaderboard_get_data(unsigned int* data_length);

/**
 * Returns on which place player ended up.
 *
 * @param leaderBoardRow
 * @return
 */
unsigned int leaderboard_push_game_data(LeaderBoardRow leaderBoardRow);


#endif //TETRIS_LEADERBOARD_H
