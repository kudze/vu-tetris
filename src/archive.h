//
// Created by karol on 2019-11-27.
//

#ifndef TETRIS_ARCHIVE_H
#define TETRIS_ARCHIVE_H

/**
 * Only loads if save exists.
 */
void archive_try_load();
void archive_save();
void archive_remove();

#endif //TETRIS_ARCHIVE_H
