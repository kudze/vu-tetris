//
// Created by karol on 2019-12-13.
//
#include "stdafx.h"

FILE *log_file = NULL;
clock_t start_clock;

const char logger_severity_tag[_LOG_SEVERITY_MAX][5] = {
    "LOG",
    "INFO",
    "WARN",
    "ERROR"
};

void logger_init() {

    start_clock = clock();
    log_file = fopen(LOG_FILE_NAME, "a");

    if (log_file == NULL)
        printf("Failed to open log file!");

    else
        logger_info("Logger has been initialized!");

    atexit(logger_deinit);

}

void logger_flush() {
    fflush(log_file);
}

void logger_deinit() {
    if(log_file != NULL) {
        clock_t end_clock = clock();
        float secondsElapsed = (float) (end_clock - start_clock) / CLOCKS_PER_SEC;
        logger_info_va("Cleaning up logger... Logger was initialized for %f seconds.", secondsElapsed);

        fclose(log_file);

        log_file = NULL;
    }
}

void logger_print_start(LogSeverity severity) {
    time_t t;
    time (&t);
    struct tm tm = *localtime(&t);

   fprintf(
        log_file,
        "%4d-%2d-%2d %2d:%2d:%2d [%s]: ",
        tm.tm_year + 1900,
        tm.tm_mon + 1,
        tm.tm_mday,
        tm.tm_hour,
        tm.tm_min,
        tm.tm_sec,
        logger_severity_tag[severity]
    );
}

void logger_log(const char *message) {
    logger_message(
        LOG_SEVERITY_LOG,
        message
    );
}

void logger_info(const char *message) {
    logger_message(
        LOG_SEVERITY_INFO,
        message
    );
}

void logger_warn(const char *message) {
    logger_message(
        LOG_SEVERITY_WARNING,
        message
    );
}

void logger_error(const char *message) {
    logger_message(
        LOG_SEVERITY_ERROR,
        message
    );
}

void logger_message(LogSeverity severity, const char *message) {
    logger_print_start(severity);

    fprintf(log_file, "%s\n", message);

    if (severity != LOG_SEVERITY_LOG)
        logger_flush();
}

void _logger_message_va(LogSeverity severity, const char* message, va_list args);

void logger_log_va(const char *message, ...) {
    va_list args;

    va_start(args, message);

    _logger_message_va(LOG_SEVERITY_LOG, message, args);

    va_end(args);
}

void logger_info_va(const char *message, ...)
{
    va_list args;

    va_start(args, message);

    _logger_message_va(LOG_SEVERITY_INFO, message, args);

    va_end(args);
}
void logger_warn_va(const char *message, ...)
{
    va_list args;

    va_start(args, message);

    _logger_message_va(LOG_SEVERITY_WARNING, message, args);

    va_end(args);
}
void logger_error_va(const char *message, ...)
{
    va_list args;

    va_start(args, message);

    _logger_message_va(LOG_SEVERITY_ERROR, message, args);

    va_end(args);
}

void logger_message_va(LogSeverity severity, const char *message, ...)
{
    va_list args;

    va_start(args, message);

    _logger_message_va(severity, message, args);

    va_end(args);
}

void _logger_message_va(LogSeverity severity, const char *message, va_list arg)
{
    logger_print_start(severity);

    vfprintf(log_file, message, arg);
    fprintf(log_file, "\n");

    if (severity != LOG_SEVERITY_LOG)
        logger_flush();
}