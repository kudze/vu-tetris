#include "stdafx.h"

LeaderBoardRow *leaderboard_data = NULL;
unsigned int leaderboard_data_length = 0;

void swap_leaderboard_row(LeaderBoardRow *a, LeaderBoardRow *b) {
    LeaderBoardRow temp = *a;
    *a = *b;
    *b = temp;
}

FILE *open_leaderboard_stream(const char *mode) {
    return fopen("leaderboard.bin", mode);
}

void leaderboard_init() {
    FILE *leaderboardFile = open_leaderboard_stream("rb");

    if (leaderboardFile == NULL)
        return;

    fread(&leaderboard_data_length, sizeof(unsigned int), 1, leaderboardFile);

    leaderboard_data = malloc(sizeof(LeaderBoardRow) * leaderboard_data_length);
    fread(leaderboard_data, sizeof(LeaderBoardRow), leaderboard_data_length, leaderboardFile);

    fclose(leaderboardFile);
}

void leaderboard_deinit() {
    if(leaderboard_data != NULL)
        free(leaderboard_data);

    leaderboard_data = NULL;
    leaderboard_data_length = 0;
}

LeaderBoardRow *leaderboard_get_data(unsigned int *data_length) {
    *data_length = leaderboard_data_length;

    return leaderboard_data;
}

unsigned int leaderboard_push_game_data(LeaderBoardRow leaderBoardRow) {
    leaderboard_data_length++;

    if (leaderboard_data == NULL)
        leaderboard_data = malloc(sizeof(LeaderBoardRow) * leaderboard_data_length);

    else
        leaderboard_data = realloc(leaderboard_data, sizeof(LeaderBoardRow) * leaderboard_data_length);

    leaderboard_data[leaderboard_data_length - 1] = leaderBoardRow;

    unsigned int result = 1;
    for (int i = leaderboard_data_length - 1; i-- > 0;) {
        if (leaderboard_data[i].score < leaderboard_data[i + 1].score)
            swap_leaderboard_row(&leaderboard_data[i], &leaderboard_data[i + 1]);
        else {
            result = i + 2;

            break;
        }
    }

    FILE *leaderboardFile = open_leaderboard_stream("wb");
    fwrite(&leaderboard_data_length, sizeof(unsigned int), 1, leaderboardFile);
    fwrite(leaderboard_data, sizeof(LeaderBoardRow), leaderboard_data_length, leaderboardFile);
    fclose(leaderboardFile);

    return result;
}