#ifndef TETRIS_LOGIC_H
#define TETRIS_LOGIC_H

#include <stdbool.h>
#include <limits.h>

/***********************************************
 * Block logic.
 ***********************************************/
typedef unsigned char game_block_colour_t;
#define GAME_BLOCK_COLOR_CYAN 0
#define GAME_BLOCK_COLOR_YELLOW 1
#define GAME_BLOCK_COLOR_PURPLE 2
#define GAME_BLOCK_COLOR_GREEN 3
#define GAME_BLOCK_COLOR_RED 4
#define GAME_BLOCK_COLOR_BLUE 5
#define GAME_BLOCK_COLOR_ORANGE 6
#define GAME_BLOCK_COLOR_NONE 7
#define GAME_BLOCK_COLOR_MAX 7

game_block_colour_t game_generate_random_block_color();

/***********************************************
 * Board Logic
 ***********************************************/
#include "board.h"

/***********************************************
 * Shape Logic
 ***********************************************/
typedef unsigned char game_shape_t;
#define GAME_SHAPE_MIN 0
#define GAME_SHAPE_I 0 //4x1
#define GAME_SHAPE_O 1 //2x2
#define GAME_SHAPE_T 2 //One mid x three line
#define GAME_SHAPE_J 3 //One left bot three line top.
#define GAME_SHAPE_L 4 //One right bot three line top
#define GAME_SHAPE_S 5 //2x2 (not alligned)
#define GAME_SHAPE_Z 6 //2x2 (not alligned) mirrored
#define GAME_SHAPE_MAX 7
#define GAME_SHAPE_UNUSED UCHAR_MAX

#define BLOCK_COUNT_PER_SHAPE 4
typedef struct {
    Point block_coordinates[BLOCK_COUNT_PER_SHAPE]; //Coordinates of tetris block.
    Point rotation_anchor;
    game_shape_t shapeIndex;
} Shape;

coord_t game_calculate_shape_width(Shape const* shape);
coord_t game_calculate_shape_height(Shape const* shape);
Point game_calculate_shape_dimensions(Shape const* shape);

/**
 * Drop rate is measurement in seconds.
 * Shows how long for shape it takes to drop one block.
 */
#define GAME_SHAPE_DROP_RATE_DEFAUT 0.4
#define GAME_SHAPE_DROP_RATE_INCREASES_AFTER_X_LINES 5
#define GAME_SHAPE_DROP_RATE_INCREASES_BY 0.05
double game_get_shape_drop_rate();
void game_set_shape_drop_rate(double rate);
Shape const* game_get_shape_of_index(game_shape_t index);
bool game_is_pos_in_shape(Shape const* shape, coord_t x, coord_t y);
bool game_is_pos_in_shape_until_index(Shape const* shape, coord_t x, coord_t y, unsigned int index);
int game_generate_random_shape();

bool game_exists_current_dropping_shape();
Shape* game_get_current_dropping_shape();
void game_generate_random_current_dropping_shape();
void game_generate_current_shape(game_shape_t shape, game_block_colour_t color);
void game_update_current_shape();
void game_clear_current_shape();

bool game_can_current_shape_be_translated(s_coord_t x, s_coord_t y);
void game_translate_current_shape(s_coord_t x, s_coord_t y);
bool game_can_current_shape_shift_to_left();
void game_shift_current_shape_to_left();
bool game_can_current_shape_shift_to_right();
void game_shift_current_shape_to_right();
bool game_can_current_shape_be_lowered(); //checks if next y is free.
void game_lower_current_shape(); //drops shape by one
void game_drop_current_shape(); //drops the shape.

typedef enum {ROTATION_STATE_LEFT, ROTATION_STATE_RIGHT, ROTATION_STATE_INVERSE} ROTATION_STATE;

bool game_can_current_shape_rotate(ROTATION_STATE rotation);
void game_rotate_current_shape(ROTATION_STATE rotation);

/***********************************************
 * SCORE
 ***********************************************/
typedef unsigned int score_t;

score_t game_get_score();
void game_add_score(score_t score);
void game_set_score(score_t score);

/**********************************************
 * SAVEGAME
 **********************************************/
#define GAME_TITLE_MAX_LENGTH 20
const char* game_get_title();

/***********************************************
 * PAUSE
 ***********************************************/
bool game_is_paused();
void game_set_paused(bool paused);

/***********************************************
 * RUNTIME
 ***********************************************/
bool game_has_ended();
void game_stop_on_next_frame();

/***********************************************
 * GAME
 ***********************************************/
void game_init(const char *title);
void game_tick(double delta);

#endif //TETRIS_LOGIC_H
