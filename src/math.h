//
// Created by karol on 2019-11-24.
//

#ifndef TETRIS_MATH_H
#define TETRIS_MATH_H

//i want templates...
unsigned int umin(unsigned int a, unsigned int b);
unsigned int umax(unsigned int a, unsigned int b);

int min(int a, int b);
int max(int a, int b);
int random(int min, int max); //min and max can appear in random value.

#endif //TETRIS_MATH_H
