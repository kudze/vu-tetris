#include "stdafx.h"

typedef vec_t(input_listener_ft) input_listener_vec_t;
input_listener_vec_t input_listeners;

void input_init() {
    vec_init(&input_listeners);
}

void input_tick() {
    int key;

    while ((key = con_read_key()) != 0) {
        if(key < 255)
            for (unsigned int i = 0; i < input_listeners.length; i++)
                input_listeners.data[i]((char) key);
    }
}

void input_deinit() {
    vec_deinit(&input_listeners);
}

void input_register_listener(input_listener_ft listener)
{
    vec_push(&input_listeners, listener);
}
