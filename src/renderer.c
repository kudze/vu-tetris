#include "stdafx.h"


#define SIDEMENU_WIDTH 40
#define LEADERBOARD_WIDTH 40
#define VIEWPORT_WIDTH BOARD_WIDTH + SIDEMENU_WIDTH + LEADERBOARD_WIDTH + 4
#define VIEWPORT_HEIGHT BOARD_HEIGHT

void renderMultiLineText(unsigned int x, unsigned int y, const char *message, unsigned int yMargin) {
    //We want to make a copy of input text.
    char _message[512];
    strncpy(_message, message, 512);

    char *line;
    unsigned int i = 0;
    yMargin++;

    line = strtok(_message, "\n");
    while (line != NULL) {
        con_set_pos((int) x, (int) (y + (i * yMargin)));
        printf("%s", line);

        line = strtok(NULL, "\n");
        i++;
    }
}

void renderLayout() {

    con_set_color(COLOR_RED, COLOR_RED);
    for (int y = 0; y < VIEWPORT_HEIGHT + 2; y++) {
        con_set_pos(0, y);

        if (y == 0 || y == VIEWPORT_HEIGHT + 1) {
            int maxX = is_leaderboard_rendered() ? VIEWPORT_WIDTH + 2 : VIEWPORT_WIDTH - LEADERBOARD_WIDTH - 1;

            for (int x = 0; x < maxX; x++)
                printf("X");
        } else {
            printf("X");

            con_set_pos(BOARD_WIDTH + 1, y);
            printf("X");

            con_set_pos(BOARD_WIDTH + SIDEMENU_WIDTH + 2, y);
            printf("X");

            if(is_leaderboard_rendered()) {
                con_set_pos(VIEWPORT_WIDTH + 1, y);
                printf("X");
            }
        }
    }
}

char block_colour_index_pool[] = {
    'I', 'O', 'T',
    'S', 'Z', 'J',
    'L'
};

char get_game_block_colour_index(game_block_colour_t block) {
    if (block == GAME_BLOCK_COLOR_NONE)
        return 'X';

    return block_colour_index_pool[block];
}

int block_colour_pool[] = {
    COLOR_CYAN,
    COLOR_ORANGE,
    COLOR_MAGENTA,
    COLOR_GREEN,
    COLOR_RED,
    COLOR_BLUE,
    COLOR_ORANGE
};

int get_game_block_bg_colour(game_block_colour_t block) {
    if (block == GAME_BLOCK_COLOR_NONE)
        return COLOR_BLACK;

    return block_colour_pool[block];
}

int get_game_block_fg_colour(game_block_colour_t block) {
    if (block == GAME_BLOCK_COLOR_NONE)
        return COLOR_BLACK;

    return COLOR_GRAY;
}

/**
 * Renders block at current position
 **/
void render_block(game_block_colour_t block) {
    char colour_index = get_game_block_colour_index(block);
    int bg_colour = get_game_block_bg_colour(block);
    int fg_colour = get_game_block_fg_colour(block);

    con_set_color(bg_colour, fg_colour);

    printf("%c", colour_index);
}

void render_board() {
    for (int y = 0; y < BOARD_HEIGHT; y++) {
        con_set_pos(1, BOARD_HEIGHT - y);

        for (int x = 0; x < BOARD_WIDTH; x++)
            render_block(board_get_block_color_at(x, y));
    }
}

void render_sidemenu(int fps) {
    char sideMenuText[512];
    sprintf(
        sideMenuText,
        "Tetris game v0.0.1\n"
        "ALPHA Testing version\n"
        "Made by Karolis Kraujelis (Kudze)\n"
        "FPS: %d\n"
        "Score: %u\n"
        "Paused: %d\n"
        "Game Title: %s\n \n"
        "Controls:\n"
        "ASD - to move active figure\n"
        "<> - to rotate active figure\n"
        "Space - to drop current figure\n"
        "P - to pause the game\n"
        "L - to show leaderboard",
        fps,
        game_get_score(),
        game_is_paused(),
        game_get_title()
    );

    con_set_color(COLOR_BLACK, COLOR_BLUE);

    renderMultiLineText(
        BOARD_WIDTH + 3,
        2,
        sideMenuText,
        1
    );

}

bool do_render_leaderboard = false;
void set_leaderboard_rendering_enabled(bool enabled) {
    do_render_leaderboard = enabled;

    con_clear();
}

bool is_leaderboard_rendered() {
    return do_render_leaderboard;
}

void render_leaderboard() {

    con_set_color(COLOR_BLACK, COLOR_BLUE);

    con_set_pos(BOARD_WIDTH + SIDEMENU_WIDTH + 4, 2);
    printf("Leaderboard:\n");

    con_set_pos(BOARD_WIDTH + SIDEMENU_WIDTH + 4, 4);
    printf("|###| %20s | %9s |", "Game title", "Score");

    unsigned int data_length;
    LeaderBoardRow* data = leaderboard_get_data(&data_length);

    if(data_length == 0)
        return;

    unsigned int maxI = umin(data_length, BOARD_HEIGHT - 5);
    for(unsigned int i = 0; i < maxI; i++) {
        con_set_pos(BOARD_WIDTH + SIDEMENU_WIDTH + 4, i + 5);
        printf("|%3d| %20s | %9u |", i + 1, data[i].player_name, data[i].score);
    }

}

int lastFPS = 0;
int currFPS = 0;
double rendDeltaSum = 0;

int calculateFPS(double delta) {
    rendDeltaSum += delta;
    currFPS++;

    if (rendDeltaSum >= 1) {
        rendDeltaSum -= 1;

        lastFPS = currFPS;
        currFPS = 0;
    }

    return lastFPS;
}

void render(double delta) {
    //Causes screen to blink.
    //con_clear();

    int fps = calculateFPS(delta);

    renderLayout();
    render_board();
    render_sidemenu(fps);

    if(is_leaderboard_rendered())
        render_leaderboard();
}

