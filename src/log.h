//
// Created by karol on 2019-12-13.
//

#ifndef TETRIS_LOG_H
#define TETRIS_LOG_H

typedef enum {
    LOG_SEVERITY_LOG,
    LOG_SEVERITY_INFO,
    LOG_SEVERITY_WARNING,
    LOG_SEVERITY_ERROR,
    _LOG_SEVERITY_MAX //<-- Is not meant for outside usage.
} LogSeverity;

#define LOG_FILE_NAME "tetris.log"

void logger_init();
void logger_flush(); // Flushes logged data into a file.
void logger_deinit();

void logger_log(const char* message); //Is not flushed to a file instantly.
void logger_info(const char* message); //Is flushed instantly
void logger_warn(const char* message); //Is flushed instantly
void logger_error(const char* message); //Is flushed instantly
void logger_message(LogSeverity severity, const char* message);

void logger_log_va(const char* message, ...); //Is not flushed to a file instantly.
void logger_info_va(const char* message, ...); //Is flushed instantly
void logger_warn_va(const char* message, ...); //Is flushed instantly
void logger_error_va(const char* message, ...); //Is flushed instantly
void logger_message_va(LogSeverity severity, const char* message, ...);

#endif //TETRIS_LOG_H
