#include "stdafx.h"

#ifdef UNIT_TEST

void test_game_add_score() {
    printf("\ntest_game_add_score unit test is running...\n");

    game_set_score(0);
    assert(game_get_score() == 0);

    game_add_score(200);
    assert(game_get_score() == 200);

    game_add_score(0);
    assert(game_get_score() == 200);

    game_add_score(-100);
    assert(game_get_score() == 100);
}

void test_game_set_score() {
    printf("\ntest_game_add_score unit test is running...\n");

    game_set_score(0);
    assert(game_get_score() == 0);

    game_set_score(2000);
    assert(game_get_score() == 2000);

    game_set_score(-2000);
    assert(game_get_score() == -2000);
}

void test_game_calculate_shape_dimensions()
{
    printf("\ntest_game_calculate_shape_dimensions unit test is running...\n");

    Point results[GAME_SHAPE_MAX] = {
        //GAME_SHAPE_I 0 //4x1
        {
            4,
            1
        },
        //GAME_SHAPE_O 1 //2x2
        {
            2,
            2
        },
        //GAME_SHAPE_T 2 //One mid x three line
        {
            3,
            2
        },
        //GAME_SHAPE_J 3 //One left bot three line top.
        {
            3,
            2
        },
        //GAME_SHAPE_L 4 //One right bot three line top
        {
            3,
            2
        },
        //GAME_SHAPE_S 5 //2x2 (not alligned)
        {
            3,
            2
        },
        //GAME_SHAPE_Z 6 //2x2 (not alligned) mirrored
        {
            3,
            2
        }
    };

    for(int i = 0; i < GAME_SHAPE_MAX; i++) {
        printf("Testing shape no. %d...\n", i);

        Point p = game_calculate_shape_dimensions(game_get_shape_of_index(i));

        assert(p.x == results[i].x && p.y == results[i].y);
    }

    printf("Testing null shape...\n");

    Point p = game_calculate_shape_dimensions(NULL);

    assert(p.x == 0 && p.y == 0);

}

int main(int argc, char* argv[]) {
    printf("\nRunning unit tests...!\n");

    test_game_calculate_shape_dimensions();
    test_game_add_score();
    test_game_set_score();

    printf("\nAll unit tests were executed successfully!\n");

    system("PAUSE");
}

#endif