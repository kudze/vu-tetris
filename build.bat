mkdir build_no_cmake

gcc -c -o build_no_cmake/con_lib.o vendor/con_lib/con_lib.c
ar rcs build_no_cmake/con_lib.lib build_no_cmake/con_lib.o

gcc -c -o build_no_cmake/vec.o vendor/vector/vec.c
ar rcs build_no_cmake/vec.lib build_no_cmake/vec.o

gcc -c -o build_no_cmake/archive.o src/archive.c -I vendor/con_lib -I vendor/vector
gcc -c -o build_no_cmake/leaderboard.o src/leaderboard.c -I vendor/con_lib -I vendor/vector

gcc build_no_cmake/leaderboard.o build_no_cmake/archive.o src/board.c src/input.c src/log.c src/logic.c src/math.c src/renderer.c src/tetris.c src/tests.c build_no_cmake/con_lib.lib build_no_cmake/vec.lib -o build_no_cmake/tetris.exe -I vendor/con_lib -I vendor/vector
gcc build_no_cmake/leaderboard.o build_no_cmake/archive.o src/board.c src/input.c src/log.c src/logic.c src/math.c src/renderer.c src/tetris.c src/tests.c build_no_cmake/con_lib.lib build_no_cmake/vec.lib -o build_no_cmake/unit_tests.exe -I vendor/con_lib -I vendor/vector -D UNIT_TEST

PAUSE